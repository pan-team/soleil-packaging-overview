# SOLEIL-Packaging-Overview

|[**Synchrotron Soleil**](https://www.synchrotron-soleil.fr/en)| [Debian PaN](https://blends.debian.org/pan/tasks/) |
|---|---|
|![Synchrotron SOLEIL](https://www.synchrotron-soleil.fr/sites/default/files/logo_0.png) | ![Debian PaN](Debian-pan-blend.png) |

This repository contains information about the packages we are pushing at [**Synchrotron Soleil**](https://www.synchrotron-soleil.fr/en).
They complement the more global effort of the [Debian Science Team](https://wiki.debian.org/DebianScience/) and its [Salsa project](https://salsa.debian.org/science-team) (also have a look at the [Python team](https://salsa.debian.org/python-team) activity). We use the [pan-maintainers](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-pan-maintainers) mailing list to keep track of our packaging efforts. You are welcome to register. In the end, we wish to push the produced packages into the [Pan Blend Tasks](https://blends.debian.org/pan/tasks/) (see [Salsa git repo](https://salsa.debian.org/blends-team/pan)).

If you wish to contribute please start here: [Pan Blend Bugs](https://blends.debian.org/pan/bugs/)

Our packagers:
- [Roland Mas](https://salsa.debian.org/lolando) / [GNURANDAL](https://gnurandal.com/pages/home/) [DDPO](https://qa.debian.org/developer.php?login=lolando%40debian.org)
- [Xavier Guimard](https://salsa.debian.org/yadd) / [DDPO](https://qa.debian.org/developer.php?login=yadd%40debian.org)
- [Frédéric-Emmanuel Picca](https://salsa.debian.org/picca) / SOLEIL [DDPO](https://qa.debian.org/developer.php?login=picca%40debian.org)
- [Emmanuel Farhi](https://nm.debian.org/person/farhi/) [DDPO](https://qa.debian.org/developer.php?login=emmanuel.farhi.1%40gmail.com)

Former packagers:
- [Neil Williams](https://salsa.debian.org/codehelp) / [Freexian](https://www.freexian.com/) [DDPO](https://qa.debian.org/developer.php?login=codehelp%40debian.org)
- [Sebastien Delafond](https://salsa.debian.org/seb) / [Freexian](https://www.freexian.com/) [DDPO](https://qa.debian.org/developer.php?login=seb%40debian.org)

---

Table of content
1. [What is on-the-way](#current)
2. [Future packages: HIGH priority](#future-high)
3. [Future packages: Medium priority](#future-medium)
4. [Future packages: low priority](#future-low)
5. [Future packages: binary only](#future-binary)
6. [Already available](#done)

---

Rationale
---
To be able to help our numerous X-ray beam-lines, the SOLEIL Data
Reduction and Analysis Group has adopted a methodology centered on a
coherent distribution of scientific software in repositories.  The
Debian system provides, to date, the most extensive scientific
software package list. This allows a seamless installation and
maintenance on our data treatment machines, as well as an easy
configuration of any derived virtual machine, Docker container and
more generally "web" micro-services.


<a name="current"></a>
What is on-the-way (WIP, NEW)
---

| Package (upstream) | Category | Debian Link | Languages / Status   | Remarks                 |
|--------------------|----------|-------------|----------------------|-------------------------|
| [xsocs](https://gitlab.esrf.fr/kmap/xsocs)            | Diffraction (surface) | https://salsa.debian.org/science-team/xsocs       | Python / WIP  | [F. Picca](https://salsa.debian.org/users/picca) |
| [crystfel](https://www.desy.de/~twhite/crystfel/)     | MX (FEL) | https://salsa.debian.org/science-team/crystfel | C / WIP, DebianPAN: MX  | not very active |
| [eiger2cbf](https://github.com/biochem-fan/eiger2cbf) | MX, I/O, HDF5 | https://salsa.debian.org/farhi-guest/eiger2cbf    | C / WIP   | [E. Farhi](https://salsa.debian.org/farhi-guest) |
| [bokeh](https://github.com/bokeh/bokeh) | lib Jupyter plots | https://salsa.debian.org/python-team/packages/python-bokeh | Python, JavaScript / [WIP](https://salsa.debian.org/python-team/packages/python-bokeh), unpackaged nodejs dependencies are a problem | A Tille |
| [**dynasor**](https://dynasor.materialsmodeling.org/index.html) | MD analysis | https://gitlab.com/materials-modeling/dynasor.git | HIGH, [WIP](https://salsa.debian.org/python-team/packages/python-dynasor) | MD trajectory analysis |
| **crysfml** | Diffraction | https://code.ill.fr/scientific-software/crysfml | HIGH / Fortran, [WIP](https://salsa.debian.org/science-team/crysfml) | Fortran Library. Build with `mkdir build; cmake -DPYTHON_API=ON -DCMAKE_Fortran_COMPILER=gfortran ..; make; make install` |
| [**eman2**](https://blake.bcm.edu/emanwiki/EMAN2) | image processing | https://github.com/cryoem/eman2 | HIGH, [WIP](https://salsa.debian.org/science-team/eman2) / python/C++ | Cryo-EM image processing |
| [**napari plugins**](https://napari.org/dev/plugins/index.html) | Viz | https://salsa.debian.org/python-team/packages/napari | HIGH / Python, [WIP](https://salsa.debian.org/python-team/packages/napari-plugin-manager) | **extensions for Napari do not install** (req. local pip / see Plugins menu) |
| [**gsas2**](https://advancedphotonsource.github.io/GSAS-II-tutorials/) | Diffraction(powder) |  https://salsa.debian.org/science-team/gsas-ii  | HIGH / Python Fortran |  PyGSAS aka GSAS-II partly ported to Debian. Initial work done but not pushed to NEW yet. |

<a name="future-high"></a>
Future packages we are considering: HIGH priority
---

| Package       | Category | Link | Priority/Language     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| [Micro-SAM](https://github.com/computational-cell-analytics/micro-sam) | segmentation | https://github.com/computational-cell-analytics/micro-sam | HIGH PyTorch | Relies on pytorch-cuda |
| [McStasScript](https://github.com/PaNOSC-ViNYL/McStasScript) | BL simulation/Jupyter NB | https://github.com/PaNOSC-ViNYL/McStasScript | HIGH / Python | use [mccode](https://salsa.debian.org/science-team/mccode) via a Notebook. |
| [PySAXS](https://pypi.org/project/pySAXS) | SAXS data reduction | https://pypi.org/project/pySAXS | HIGH / Python | source code distributed as a wheel, waiting for author feedback |
| [mpes](https://github.com/mpes-kit/mpes) | XPS/ARPES | https://github.com/mpes-kit/mpes | HIGH / Python, WIP, lots of unpackaged dependencies | |
| [itkwidgets](https://github.com/InsightSoftwareConsortium/itkwidgets) | Notebook widget | https://github.com/InsightSoftwareConsortium/itkwidgets | HIGH / Python/NodeJS | Volume renderer for python notebooks |
| [cadquery](https://cadquery.readthedocs.io) | I/O | https://github.com/CadQuery/cadquery | HIGH Python | CAD formats / opencascade/McCode |
| mdanse (ex nMoldyn) | Spectroscopy | https://github.com/ISISNeutronMuon/MDANSE | HIGH / C, Python |  MD to S(q,w) and more. |


<a name="future-medium"></a>
Future packages we are considering: Medium priority
---

| Package       | Category | Link | Priority/Language   | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| refmac5 | MX | [refmac tgz](https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/refmac/SourceEtal/refmac5.8_v0091.tgz) | Medium / Fortran | |
| xdsme | MX | https://github.com/legrandp/xdsme | Medium / Python |  DebianPAN: MX |
| mxcube | MX | https://github.com/mxcube/mxcube | Medium / Python |  DebianPAN: MX https://github.com/mxcube/mxcubecore and https://github.com/mxcube/mxcubeqt |
| arpes | ARPES | https://github.com/chstan/arpes | Medium / Python | extensive, runs from Jupyter NB, lots of unpackaged dependencies |
| [tomoj](https://sourceforge.net/projects/tomoj/)    | Tomo | https://sourceforge.net/projects/tomoj/     | HIGH / Java | ImageJ plugin |
| [clij](https://github.com/clij/clij-docs)     | Imaging | https://github.com/clij/clij-docs        | HIGH / Java, C, Python | ImageJ/Fiji plugin |
| [RootPainter](https://github.com/Abe404/root_painter) | U-Net / Torch | https://github.com/Abe404/root_painter | HIGH / Python | Relies on pytorch-cuda |


<a name="future-low"></a>
Future packages we are considering: low priority
---

| Package       | Category | Link | Priority/Status     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| [fiji](https://fiji.sc/)     | imaging GUI | https://fiji.sc/                     | LOW |            ImageJ upgrade, **too complex**, 50+ Java dependencies |
| nebula | Diffraction |https://github.com/AndreasReiten/nebula          | Low  | **Untouched/5y**  DebianPAN: Diffraction?|
| bonsu | CDI | https://github.com/bonsudev/bonsu                 | Low |  DebianPAN: Coherent-diffraction |
| hawk  |CDI |http://xray.bmc.uu.se/hawk/?q=hawk/whatishawk     | Low|  DebianPAN: Coherent-diffraction |
| phon | spectroscopy (vibrational)  | http://www.homepages.ucl.ac.uk/~ucfbdxa/phon/      | Low |  A program to calculate phonons using the small displacement method |
| rescal | neutron spectroscopy | [rescal doc/src](http://ifit.mccode.org/Applications/ResLibCal/doc/ResLibCal.html) | Low  | Legacy Neutron TAS resolution calculation, simple Fortran code |
| spinwave | spin-wave spectroscopy | http://www-llb.cea.fr/logicielsllb/SpinWave/SW.html | Low |  Compute spin-wave dispersions (CEA/LLB), in fortran |
| spectra | Source modelling| http://spectrax.org/spectra/ | Low | source sim |
| simplex | Source modelling| http://spectrax.org/simplex/index.html |Low  | source sim |
| genesis| Source modelling | http://genesis.web.psi.ch/  | Low |  source sim |
| oasys/orange3 | BL modelling | https://github.com/oasys-kit | Low | source and BL modelling |
| srw | BL modelling | https://github.com/ochubar/SRW | Low | source sim |
| simex | BL modelling | https://github.com/PaNOSC-ViNYL/SimEx | Low | XFEL simulation |
| mlfsom | MX modelling | https://bl831.als.lbl.gov/~jamesh/mlfsom/ | Low | MX simulation |
| idl2matlab | misc (converter) | https://github.com/farhi/idl2matlab | Low |  Convert IDL code into Matlab (flex/bison). Can be tricky (old C) |
| python3-extranormal3 | XANES/EXAFS |https://pypi.org/project/extranormal3/     | Low |  Postponed, python lib |
| [jana 2000](http://jana.fzu.cz/) | Diffraction | http://jana.fzu.cz/                 | Low | diff powder, alt. for FullProf . Not much used according to Michal Dusek dusek@fzu.cz |
| [ipyvolume](https://github.com/widgetti/ipyvolume) | Notebook widget | https://github.com/widgetti/ipyvolume | Low / Python/NodeJS | Volume renderer for python notebooks. Probably deprecated |
| [pyhst2](http://ftp.esrf.fr/scisoft/PYHST2/)          | Tomo | https://salsa.debian.org/science-team/pyhst2      | Low, in [SID](https://packages.debian.org/sid/python3-pyhst2-cuda)    | upgrade upstream to current git 2023, issue with CUDA12, **end-of-life** |
| HipGISAXS | SAXS | https://github.com/HipGISAXS/HipGISAXS | Low | 3yrs old |

<a name="future-binary"></a>
Software that can only be distributed as binary (contrib/non-free) or have Licensing issues.
---

| Package       | Category | Link | Priority/Status     | Remarks                    |
|---------------|----------|------|---------------------|----------------------------|
| MAUD | Diffraction | http://maud.radiographema.eu/ | Low | |
| FullProf | Diffraction | https://www.ill.eu/sites/fullprof/ | Low | based on CrysFML, no source code avail. |
| ANA-ROD | Diffraction (surface) | http://www.esrf.eu/computing/scientific/joint_projects/ANA-ROD/ | Low | can not distribute src. Use Numerical recipies for L-M fit. May use [lmfit](https://jugit.fz-juelich.de/mlz/lmfit) or [MinPack](https://packages.debian.org/source/sid/minpack) as a replacement. |
| pyROD | Diffraction (surface) | http://zhoutao.eu/pyrod/ | Low | deprecated dependencies (gui) |
| ATSAS | SAXS | https://www.embl-hamburg.de/biosaxs/download.html | Low | need registration |
| ShelX | MX | http://shelx.uni-goettingen.de/ | Low | need registration |
| fit2d | Diffraction | https://gitlab.esrf.fr/hammersl/fit2d | Low | old but still highly used |
| pynx | ptychography | http://www.esrf.eu/computing/scientific/PyNX/README.html | Low | issue with license/patent |
| fdmnes | EXAFS | http://neel.cnrs.fr/spip.php?article3137 | Low | no src |
| [bcdi](https://pypi.org/project/bcdi/) | Bragg CDI | http://salsa.debian.org/science-team/bcdi | HIGH | Bragg CDI/ptycho, requires PyNX which is non-free |
| adxv | MX | https://www.scripps.edu/tainer/arvai/adxv.html | Low | no src |
| [mosflm](https://www.mrc-lmb.cam.ac.uk/mosflm/mosflm/)   | MX | https://www.mrc-lmb.cam.ac.uk/mosflm/mosflm/  | Low | MX simulation, issue with License |









-------------------------------------------------------------------------------------------------------------------

<a name="done"></a>
Packages already in Debian (DONE)
---

| Package       | Category | Debian Repo |  deb | Remarks                    |
|---------------|----------|-------------|------|----------------------------|
| [tomwer](https://gitlab.esrf.fr/tomotools/tomwer) | Tomo/Orange3 | https://gitlab.esrf.fr/tomotools/tomwer | HIGH, in [SID](https://tracker.debian.org/pkg/tomwer) | requires Orange3 and Nabu |
| [devscripts](https://salsa.debian.org/debian/devscripts) | packaging | from https://salsa.debian.org/science-team/deb2container | in [SID](https://tracker.debian.org/pkg/devscripts) | convert deb package to apptainer | 
| [c-blosc2](https://github.com/Blosc/c-blosc2) | compression | https://github.com/Blosc/c-blosc2 | in [SID](https://tracker.debian.org/pkg/c-blosc2) / C | Done by LEAPS-INNOV |
| [NexPy](https://pypi.org/project/NeXpy/) | I/O HDF5 | https://github.com/nexpy/nexpy | HIGH / Python in [SID](https://tracker.debian.org/pkg/python-nexpy) | lightweight NeXus/HDF5 viewer |
| [extranormal3](https://pypi.org/project/extranormal3/) | EXAFS | https://pypi.org/project/extranormal3/ | HIGH Python in [SID](https://tracker.debian.org/pkg/python-extranormal3) | For Orange3/spectroscopy/Quasar. Very simple. Deps: numpy,scipy |
| [rsshfs](https://blog.rom1v.com/2014/06/sshfs-inverse-rsshfs/) | I/O | https://github.com/rom1v/rsshfs | HIGH bash, in [SID](https://tracker.debian.org/pkg/rsshfs) | I/O reverse sshfs |
| [NXtomo](https://gitlab.esrf.fr/tomotools/nxtomo) | API for tomo | https://gitlab.esrf.fr/tomotools/nxtomo | HIGH / Python in [SID](https://tracker.debian.org/pkg/nxtomo)| For tomography |
| [StackView](https://github.com/haesleinhuepf/stackview) | NB Widget | https://github.com/haesleinhuepf/stackview | HIGH / Python in [SID](https://tracker.debian.org/pkg/stackview) | For notebooks |
| [plotpy](https://pypi.org/project/PlotPy/)  | GUI/plotting  | https://pypi.org/project/PlotPy/ | HIGH / Python in [SID](https://tracker.debian.org/pkg/plotpy) | Future dependency for DataLab |
| [DataLab](https://github.com/DataLab-Platform/DataLab) | GUI in the style of Silx | https://salsa.debian.org/python-team/packages/datalab | HIGH / Python in [SID](https://tracker.debian.org/pkg/datalab) | Dependency Added  . Uses PlotPy |
| [xypattern](https://pypi.org/project/xypattern) | diffraction | https://github.com/CPrescher/xypattern | HIGH python in [SID](https://tracker.debian.org/pkg/python-xypattern) | Dependency for dioptas |
| [cdlclient](https://github.com/DataLab-Platform/DataLabSimpleClient) | I/O | https://github.com/DataLab-Platform/DataLabSimpleClient | HIGH / Python in [SID](https://tracker.debian.org/pkg/cdlclient) | Diffraction for Dioptas |
| [TabNet](https://pypi.org/project/pytorch-tabnet/) | DL pytorch | https://github.com/dreamquark-ai/tabnet | HIGH / Python in [SID](https://tracker.debian.org/pkg/tabnet) | Relies on pytorch-cuda |
| [coot](https://www2.mrc-lmb.cam.ac.uk/personal/pemsley/coot/) | MX GUI  | https://salsa.debian.org/science-team/coot | C, C++, Python in [SID](https://tracker.debian.org/pkg/coot) | Andrius Merkys |
| [mccode](https://github.com/McStasMcXtrace/McCode) | BL modelling (neutrons/X) | https://salsa.debian.org/science-team/mccode | Fortran, C, Python, in [SID](https://tracker.debian.org/pkg/mccode) | McStas/McXtrace |
| [PyNX](https://gitlab.esrf.fr/favre/PyNX) | Ptychography | https://gitlab.esrf.fr/favre/PyNX | in [SID](https://tracker.debian.org/pkg/pynx) | For X-ray CDI/ptycho |
| [ipydatagrid](https://github.com/bloomberg/ipydatagrid) | Notebook widget | https://github.com/bloomberg/ipydatagrid | Python, TypeScript in [SID](https://tracker.debian.org/pkg/node-ipydatagrid) | Excel-style spreadsheet in Jupyter/NB, replaces [ipysheet](https://ipysheet.readthedocs.io/en/stable/) |
| [igor2](https://github.com/AFM-analysis/igor2/) | I/O | https://salsa.debian.org/python-team/packages/igor2  | in [SID](https://packages.debian.org/source/unstable/igor2) | Replaces Igor, used by NavARP |
| [ovito](https://gitlab.com/stuko/ovito) | Material modeling viewer | https://gitlab.com/stuko/ovito | Python / in [EXP](https://tracker.debian.org/pkg/ovito)| Material modeling viewer |
| [quantum-espresso-data-sssp](https://www.materialscloud.org/discover/sssp/table/efficiency) and [archive](https://archive.materialscloud.org/record/2023.65) | material simulation | https://www.materialscloud.org/discover/sssp/table/efficiency | data, in [SID](https://tracker.debian.org/pkg/quantum-espresso-data-sssp) | should be placed in `/usr/share/espresso/pseudo`. Conflict/replaces with `quantum-espresso-data`. license: GPL2 and 3, Creative Commons 3 and 4 |
| [demeter](https://github.com/bruceravel/demeter) Athena/Artemis | XAS | https://salsa.debian.org/science-team/horae, https://tracker.debian.org/pkg/horae, https://salsa.debian.org/perl-team/modules/packages/libdemeter-perl| in [SID](https://packages.debian.org/source/unstable/libdemeter-perl) Perl/Fortran | update for Athena/Artemis |
| [deepdish](https://github.com/uchicago-cs/deepdish) | I/O HDF5 | https://tracker.debian.org/pkg/deepdish | Python in [SID](https://packages.debian.org/source/sid/deepdish) | HDF5 I/O tools |
| [nxmx](https://github.com/dials/nxmx) | I/O HDF5 | https://salsa.debian.org/python-team/packages/nxmx | Python in [SID](https://tracker.debian.org/pkg/nxmx) | DIALS handler for MX/HDF5 data  |
| [navarp](https://gitlab.com/fbisti/navarp)   | inelastic ARPES  | https://salsa.debian.org/science-team/navarp | HIGH / Python, in [SID](https://tracker.debian.org/pkg/navarp) | Pushed by [R. Mas](https://salsa.debian.org/lolando) |
| [Codra](https://github.com/CODRA-Ingenierie-Informatique/CodraFT) | data visualization | https://salsa.debian.org/python-team/packages/codraft | in [SID](https://tracker.debian.org/pkg/codraft) / Python | an other viewer for e.g. diffraction, but fittng is limited |
| [jupyterlab](https://github.com/jupyterlab/jupyterlab) | Notebook | https://salsa.debian.org/python-team/packages/jupyterlab.git | TypeScript, JS / WIP  in [SID](https://tracker.debian.org/pkg/jupyterlab)| Collaborative NB with extensions, hard work with many nodejs deps. `node-jupyterlab` in [EXP](https://packages.debian.org/experimental/node-jupyterlab) |
| [napari](https://napari.org/) | viz | https://salsa.debian.org/python-team/packages/napari | in [SID](https://tracker.debian.org/pkg/napari) / Python | multi-dimensional image viewer for Python. Many deps. |
| [quasar/orange3](https://github.com/Quasars/orange-spectroscopy) | spectroscopy GUI | https://salsa.debian.org/science-team/orange-spectroscopy | in [SID](https://tracker.debian.org/pkg/orange-spectroscopy) / Python | with spectroscopy IR/UV |
| [ipywidgets](https://ipywidgets.readthedocs.io/en/latest/) | Notebook widgets | https://salsa.debian.org/python-team/packages/ipywidgets | TypeScript, Python / WIP in [v8 EXP](https://tracker.debian.org/pkg/ipywidgets) | Widgets for e.g. JupyterLab. Many NodeJS dependencies as listed in https://wiki.debian.org/Javascript/Nodejs/Tasks/Jupyterlab now in [EXP](https://packages.debian.org/experimental/node-jupyterlab) |
| [PyTorch](https://pytorch.org/) | IA | https://salsa.debian.org/deeplearning-team/pytorch | in [EXP](https://tracker.debian.org/pkg/pytorch-cuda) / Python - GPU | with GPU support |
| [nabu](https://gitlab.esrf.fr/tomotools/nabu) | Tomo | https://salsa.debian.org/python-team/packages/nabu | in [SID](https://tracker.debian.org/pkg/nabu) / python/cuda | Layer on top of tomo codes, compatible with PyHST2. With tomoscan (in [SID](https://tracker.debian.org/pkg/tomoscan))|
| [Tofu](https://github.com/ufo-kit/tofu) | Tomography reconstruction | https://salsa.debian.org/science-team/ufo-tofu | in [SID](https://tracker.debian.org/pkg/ufo-tofu) / Python | Not the same as [tofu in Debian](https://tracker.debian.org/pkg/tofu) which is "high-level network game engine for Python" |
| [hdf5plugin](https://pypi.org/project/hdf5plugin/) https://github.com/silx-kit/hdf5plugin | HDF5 I/O | https://salsa.debian.org/science-team/hdf5-filter-plugin | in [SID](https://tracker.debian.org/pkg/hdf5-filter-plugin) / C, C++, Python, Assembly | compression filters for HDF5. Pushed by Freexian/ESRF |
| [dials](https://dials.github.io/)                     | Diffraction | https://salsa.debian.org/science-team/dials       | in [SID](https://tracker.debian.org/pkg/dials) C++,Python  | [F. Picca](https://salsa.debian.org/users/picca) and [R. Mas](https://salsa.debian.org/lolando) |
| [mrcfile](https://github.com/ccpem/mrcfile) | I/O | https://salsa.debian.org/python-team/packages/mrcfile | in [SID](https://tracker.debian.org/pkg/python-mrcfile) / Python | Python implementation of the MRC2014 file format. Needed by CCTBX/DIALS. |
| [orderedset](https://pypi.org/project/orderedset/) | I/O | https://salsa.debian.org/python-team/packages/python-orderedset | in [SID](https://tracker.debian.org/pkg/python-orderedset) / Python | Ordered Set implementation in Cython. Needed by CCTBX/DIALS. |
| [cif2hkl](https://gitlab.com/soleil-data-treatment/soleil-software-projects/cif2hkl) | Diffraction | https://salsa.debian.org/science-team/cif2hkl | in [SID](https://tracker.debian.org/pkg/cif2hkl)  / Fortran | Compute F^2 for neutrons, X and electrons, based on FullProf/CrysFML. Code in Fortran, required by McCode. There is a Debian package creator in the project. |
| [feff85](https://github.com/xraypy/feff85exafs) | EXAFS | https://salsa.debian.org/science-team/feff85exafs | Fortran [SID](https://packages.debian.org/sid/feff85exafs) | required by xraylarch. See https://feff.phys.washington.edu/feffproject-feff-download.html |
| [pymatgen](https://pymatgen.org/) | materials analysis | https://salsa.debian.org/debichem-team/pymatgen | Python [SID](https://packages.debian.org/sid/python3-pymatgen) | similar to ASE |
| [python-pyepics](https://pyepics.github.io/pyepics/) | EPICS python bindings | https://salsa.debian.org/science-team/python-pyepics | Python / in [SID](https://tracker.debian.org/pkg/python-pyepics) | [Freexian](https://www.freexian.com/) needed by xraylarch |
| [xraylarch](https://xraypy.github.io/xraylarch/)      | XANES/EXAFS spectroscopy | https://salsa.debian.org/science-team/xraylarch | Python / in [SID](https://tracker.debian.org/pkg/xraylarch) | |
| [bioxtas-raw](https://bioxtas-raw.readthedocs.io/en/latest/) | SAXS | https://salsa.debian.org/science-team/bioxtasraw | Python / [SID](https://tracker.debian.org/pkg/bioxtasraw) | SAXS GUI |
| [pyobjcryst](https://github.com/diffpy/pyobjcryst) | diffraction | https://salsa.debian.org/science-team/pyobjcryst | C++, Python / in [SID](https://tracker.debian.org/pkg/pyobjcryst) | diff low level lib (FOX) |
| [PyImageTool](https://github.com/kgord831/PyImageTool) | ARPES data viewer | https://salsa.debian.org/science-team/pyimagetool | in [SID](https://tracker.debian.org/pkg/pyimagetool) | ARPES spectroscopy |
| [XRT](https://xrt.readthedocs.io) | Beam-line simulation | https://salsa.debian.org/science-team/xrt | in [SID](https://tracker.debian.org/pkg/python-xrt) | simulation, wave+ray-tracing  |
| [python3-AnyQt](https://pypi.org/project/AnyQt/) | GUI lib | https://salsa.debian.org/python-team/packages/python-anyqt  | in [SID](https://tracker.debian.org/pkg/python-anyqt) | python lib GUI |
| [looktxt](https://gitlab.com/soleil-data-treatment/soleil-software-projects/looktxt) | I/O | https://salsa.debian.org/science-team/looktxt  | in [SID](https://tracker.debian.org/looktxt) | format conversion text->other |
| [xraydb](https://github.com/xraypy/XrayDB) | dependency of xraylarch | https://github.com/xraypy/XrayDB | in [SID](https://tracker.debian.org/pkg/xraydb) | X-ray Reference Data |
| [libobjcryst](https://github.com/diffpy/libobjcryst) | diffraction | https://salsa.debian.org/science-team/libobjcryst | in [SID](https://tracker.debian.org/pkg/libobjcryst) | diff low level lib/FOX/pyobjcryst |
| [freesas](https://github.com/kif/freesas)  | SAS | https://github.com/kif/freesas    | in [SID](https://tracker.debian.org/pkg/freesas) | small angle scattering |
| [freeart](https://gitlab.esrf.fr/freeart/freeart)  | Tomo |  https://salsa.debian.org/python-team/packages/freeart | in [SID](https://tracker.debian.org/pkg/freeart) | DebianPAN: Tomo  |
| [threeb](http://www.coxphysics.com/3b/index.html)   | Microscopy imaging | http://www.coxphysics.com/3b/index.html  | in [SID](https://tracker.debian.org/pkg/threeb) |  DebianPAN: microscopy, ImageJ plugin, with `toon` and `toontag` |
| [pyvkfft](https://github.com/vincefn/pyvkfft) | OpenCL FFT /Python | https://salsa.debian.org/science-team/pyvkfft | in [SID](https://tracker.debian.org/pkg/pyvkfft) | relies on VKFFT |
| [facet-analyzer](https://github.com/picca/FacetAnalyser) | ParaView plugin | https://salsa.debian.org/science-team/facet-analyser | in [SID](https://tracker.debian.org/pkg/facet-analyser) | for ParaView |
| [extra-data](https://github.com/European-XFEL/EXtra-data) | I/O | https://salsa.debian.org/python-team/packages/extra-data | in [SID](https://tracker.debian.org/pkg/extra-data) | read HDF5 EXFEL data |
| [mcpl](https://mctools.github.io/mcpl/) | BL simulation | https://salsa.debian.org/science-team/mcpl | in [SID](https://tracker.debian.org/pkg/mcpl) | for McCode |
| [ncrystal](https://mctools.github.io/ncrystal/) | BL simulation | https://salsa.debian.org/science-team/ncrystal | in [SID](https://tracker.debian.org/pkg/ncrystal) | for McCode |
| [mumax3](https://mumax.github.io/) | micromagnetic simulation | https://salsa.debian.org/go-team/packages/mumax3 | in [SID](https://tracker.debian.org/pkg/mumax3) |  |
| [moviepy](https://pypi.org/project/moviepy/)  | video |  https://salsa.debian.org/python-team/packages/moviepy | in [SID](https://tracker.debian.org/pkg/moviepy) | for bcdi |
| [cctbx](https://cci.lbl.gov/cctbx_docs/index.html)    | MX | https://salsa.debian.org/science-team/cctbx      | in [SID](https://tracker.debian.org/pkg/cctbx) | Computational Crystallography Toolbox |
| [horae](https://github.com/bruceravel/demeter) Athena/Artemis | XAS | https://salsa.debian.org/science-team/horae and https://tracker.debian.org/pkg/horae| in [SID](https://tracker.debian.org/pkg/horae) | |
| [tomopy]( https://tomopy.readthedocs.io/en/latest/)   |Tomo |  https://salsa.debian.org/python-team/packages/tomopy   | in [SID](https://tracker.debian.org/pkg/tomopy)     | DebianPAN: Tomography **:warning: dxchange not included. Issue with .so naming** |
| [mantis-xray](https://spectromicroscopy.com/) and [git](https://github.com/mlerotic/spectromicroscopy)   | spectro-microscopy | https://salsa.debian.org/python-team/packages/mantis-xray | in [SID](https://tracker.debian.org/pkg/mantis-xray) | Absorption microscopy |
| [tomogui](https://pypi.org/project/tomogui/)  | Tomo GUI | https://salsa.debian.org/python-team/packages/tomogui | in [SID](https://tracker.debian.org/pkg/tomogui) | Tomo interface on top of freeart  |
| [pyxrd](https://github.com/PyXRD/PyXRD) | Diffraction (powder) | https://salsa.debian.org/science-team/pyxrd  | in [SID](https://tracker.debian.org/pkg/pyxrd) |  DebianPAN: Powder |
| [dioptas](http://www.clemensprescher.com/programs/dioptas) and [git](https://github.com/Dioptas/Dioptas) | powder diffraction | https://salsa.debian.org/python-team/packages/dioptas | in [SID](https://tracker.debian.org/pkg/dioptas) | GUI on top pf PyFAI |
| [phonopy](https://phonopy.github.io/phonopy/)  | inelastic | https://salsa.debian.org/debichem-team/phonopy     | in [SID](https://tracker.debian.org/pkg/phonopy)| RIXS/XPS/ARPES by DebiChem |
| [jupyterhub](https://jupyter.org/hub) | notebook account management | https://salsa.debian.org/python-team/packages/jupyterhub | in [SID](https://tracker.debian.org/pkg/jupyterhub). All packages pushed: `jupyter-packaging jupyter-telemetry jupyterhub node-colorspace node-configurable-http-proxy node-dabh-diagnostics node-enabled node-fast-safe-stringify node-fecha node-fn.name node-http-proxy node-kuler node-logform node-lynx node-mersenne node-one-time node-stack-trace node-statsd-parser node-strftime node-text-hex node-triple-beam node-winston node-winston-transport` |
| [vkfft](https://github.com/DTolm/VkFFT) | OpenCL FFT | https://salsa.debian.org/science-team/vkfft | in [SID](https://tracker.debian.org/pkg/vkfft) |
| [dmrgpp](https://github.com/g1257/dmrgpp) (dmrg++) | ARPES | https://salsa.debian.org/science-team/dmrgpp    | in [SID](https://packages.debian.org/unstable/dmrgpp) | [Freexian](https://www.freexian.com/) Neil Williams |
| [ifeffit](https://tracker.debian.org/pkg/ifeffit) | XAS | https://salsa.debian.org/science-team/ifeffit | in [SID](https://packages.debian.org/unstable/ifeffit) | [Freexian](https://www.freexian.com/) Neil Williams |
| [xrstools](https://gitlab.esrf.fr/ixstools/xrstools/-/tree/mac) | inelastic | https://salsa.debian.org/science-team/xrstools | in [SID](https://tracker.debian.org/pkg/xrstools) | [Freexian](https://www.freexian.com/) Neil Williams |
| [navarp](https://gitlab.com/fbisti/navarp)   | inelastic ARPES  | https://salsa.debian.org/science-team/navarp | in [SID](https://tracker.debian.org/pkg/navarp) |
| [clpeak](https://github.com/krrishnarraj/clpeak) | OpenCL | https://salsa.debian.org/opencl-team/clpeak | in [SID](https://tracker.debian.org/pkg/clpeak) |
| [xraylib](https://github.com/tschoonj/xraylib) | a library for interactions of X-rays with matter | https://salsa.debian.org/science-team/xraylib | in [SID](https://tracker.debian.org/pkg/xraylib) | [Freexian](https://www.freexian.com/) python lib, highly used |
| [nbsphinx-link](https://pypi.org/project/nbsphinx-link/) | Notebook tool | https://salsa.debian.org/science-team/nbsphinx-link | in [SID](https://tracker.debian.org/pkg/nbsphinx%2Dlink) | [Freexian](https://www.freexian.com/) python lib for PyNX |
| [python3-openTSNE](https://pypi.org/project/openTSNE/) | ML |  https://salsa.debian.org/science-team/opentsne | in [SID](https://tracker.debian.org/pkg/opentsne)  | [Freexian](https://www.freexian.com/) python lib, for Orange3, highly used |
| [python3-spectral](https://github.com/spectralpython/spectral/) | multi-spectral | https://salsa.debian.org/science-team/python-spectral in [SID](https://tracker.debian.org/pkg/python-spectral) | [Freexian](https://www.freexian.com/) python lib, for Orange3 |
| [python3-pynndescent](https://github.com/lmcinnes/pynndescent) | optimization | https://salsa.debian.org/med-team/python-pynndescent.git in [SID](https://packages.debian.org/source/testing/python-pynndescent) | Debian-Med python lib |
| [python3-louvain](https://github.com/taynaud/python-louvain) | graphs  | https://salsa.debian.org/python-team/modules/python-louvain  in [SID](https://packages.debian.org/source/sid/python-louvain) | for Orange3, needs update ? |
| [qemu-web-desktop](https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop) (DARTS) | Virtual Data treatment | https://salsa.debian.org/debian/qemu-web-desktop | in [SID](https://packages.debian.org/sid/qemu-web-desktop) | Pushed by [R. Mas](https://salsa.debian.org/lolando)  |
| [pyhst2](http://ftp.esrf.fr/scisoft/PYHST2/)          | Tomo | https://salsa.debian.org/science-team/pyhst2      | in [SID](https://packages.debian.org/sid/python3-pyhst2-cuda)    | pushed by [Freexian](https://www.freexian.com/)     |
| [xrayutilities](https://xrayutilities.sourceforge.io/) | Diffraction | https://salsa.debian.org/freexian-team/xrayutilities | in [SID](https://packages.debian.org/sid/xrayutilities)  | pushed by [Freexian](https://www.freexian.com/), using previous repo from [F. Picca](https://salsa.debian.org/users/picca) |
| [astra-toolbox](https://www.astra-toolbox.com/)       | Tomo  | https://salsa.debian.org/science-team/astra-toolbox | in [SID](https://packages.debian.org/sid/python3-astra-toolbox)  | pushed by [Freexian](https://www.freexian.com/)     |
| [python3-procrunner](https://pypi.org/project/procrunner) | system | https://salsa.debian.org/science-team/python-procrunner | in [SID](https://packages.debian.org/sid/python3-procrunner) | pushed by [Freexian](https://www.freexian.com/) |
| [pyct](https://github.com/pyviz-dev/pyct/)        |  | https://salsa.debian.org/science-team/pyct  | in [SID](https://packages.debian.org/sid/python3-pyct)  | pushed by [Freexian](https://www.freexian.com/)
| [python3-colorcet](https://github.com/holoviz/colorcet) | color lib | https://salsa.debian.org/science-team/colorcet           | in [SID](https://tracker.debian.org/pkg/colorcet)  | pushed by [Freexian](https://www.freexian.com/) |
| [python3-tifffile](https://pypi.org/project/tifffile/)  | I/O | https://salsa.debian.org/python-team/packages/tifffile | in [SID](https://packages.debian.org/sid/python3-tifffile) | python lib, for HypersPy |
| [dials-data](https://pypi.org/project/dials-data/ ) | data for DIALS | https://salsa.debian.org/science-team/dials-data | in [SID](https://packages.debian.org/unstable/python3-dials-data) | python lib, for DIALS, pushed by [Freexian](https://www.freexian.com/) |
| [hyperspy](https://hyperspy.org/) | multi-spectral | https://salsa.debian.org/science-team/hyperspy                          | in [SID](https://tracker.debian.org/pkg/hyperspy)     | pushed by [Freexian](https://www.freexian.com/) |
| [denss](https://github.com/tdgrant1/denss.git) | SAXS | https://salsa.debian.org/science-team/denss | in [SID](https://tracker.debian.org/pkg/denss) |  pushed by [Freexian](https://www.freexian.com) |
| [genx](https://genx.sourceforge.io/)  | Reflectvity | https://salsa.debian.org/science-team/genx | in [SID](https://tracker.debian.org/pkg/genx) |   pushed by [Freexian](https://www.freexian.com) |
| [bornagain](https://www.bornagainproject.org/)        | Reflectivity | https://salsa.debian.org/science-team/bornagain  | in [SID](https://packages.debian.org/sid/bornagain), python3.8 bindings not functional  | pushed by [Freexian](https://www.freexian.com/) |
| [igor](http://git.tremily.us/?p=igor) | XPS/ARPES | https://salsa.debian.org/science-team/igor  | in [SID](https://tracker.debian.org/pkg/python-igor) | pushed by [Freexian](https://www.freexian.com/) |
| [arpys](https://github.com/kuadrat/arpys) | XPS/ARPES | https://salsa.debian.org/science-team/arpys  | in [SID](https://tracker.debian.org/pkg/arpys) | work by [Freexian](https://www.freexian.com/) |
| [epics-base](https://epics.anl.gov/index.php) | EPICS libraries | https://salsa.debian.org/science-team/epics-base | in [SID](https://tracker.debian.org/epics-base) | [Freexian](https://www.freexian.com/) needed by xraylarch |
| binoculars    | Diffraction (surface)  | https://salsa.debian.org/science-team/binoculars https://packages.debian.org/source/buster/binoculars | DebianPAN: 3D Diffraction |
| bitshuffle    | compression| https://salsa.debian.org/alteholz/bitshuffle  https://packages.debian.org/source/sid/bitshuffle   | |
| [cbflib](http://www.bernstein-plus-sons.com/software/CBF/)  | MX | https://salsa.debian.org/science-team/cbflib in [SID](https://packages.debian.org/source/sid/cbflib) |  [F. Picca](https://salsa.debian.org/users/picca) contribution |
| fityk         | Fitting GUI |https://salsa.debian.org/science-team/fityk  https://packages.debian.org/source/buster/fityk   | DebianPAN: Data-reduction |
| ghkl          | Diffraction| https://salsa.debian.org/science-team/hkl       | DebianPAN: control-systems |
| gwyddion      | Imaging Microscopy |https://salsa.debian.org/med-team/gwyddion   https://packages.debian.org/buster/gwyddion   | DebianPAN: Microscopy |
| h5py          | I/O HDF5 |https://salsa.debian.org/science-team/h5py   https://packages.debian.org/buster/python3-h5py    | Data-reduction |
| horae         | EXAFS | https://packages.debian.org/buster/horae  |  Athena Artemis  Hephaestus |
| imagej        | Imaging GUI |https://salsa.debian.org/med-team/imagej    https://packages.debian.org/buster/imagej     | DebianPAN: Data-reduction |
| itango        | Control |https://salsa.debian.org/science-team/itango  https://packages.debian.org/buster/python-itango  | DebianPAN: control-systems |
| labplot       | Origin clone | https://packages.debian.org/buster/labplot | Data analysis / Origin |
| libxy         | Diffraction (powder) | https://packages.debian.org/source/buster/xylib | DebianPAN: Powder |
| lz4           | compression| https://salsa.debian.org/debian/lz4  https://packages.debian.org/buster/lz4    | |
| mayavi2       | 3D Viz | https://packages.debian.org/buster/mayavi2 | DebianPAN: 3D :warning: obsolete ? |
| objcryst-fox  | Diffraction| https://packages.debian.org/sid/amd64/objcryst-fox | :warning: obsolete ? |
| octave        | Matlab-clone|https://salsa.debian.org/pkg-octave-team/octave https://packages.debian.org/buster/octave| DebianPAN: Data-reduction |
| paraview      | 3D Viz | https://salsa.debian.org/science-team/paraview https://packages.debian.org/buster/paraview  | DebianPAN: 3D |
| pyfai         | Diffraction (powder/SAXS) | https://salsa.debian.org/science-team/pyfai  https://packages.debian.org/buster/pyfai   | DebianPAN: Powder Diffraction |
| pymca         | Fluo/Imaging|https://salsa.debian.org/science-team/pymca   https://packages.debian.org/buster/pymca  | DebianPAN: Data-reduction |
| [python3-PTable](https://pypi.org/project/PTable/) | I/O | https://packages.debian.org/source/buster/ptable |  python lib, for HypersPy |
| pytango       | Control |https://salsa.debian.org/science-team/pytango  https://packages.debian.org/buster/python-pytango  | DebianPAN: control-systems |
| [Stable/python3-yaml / PyYAML](https://pypi.org/project/PyYAML/) | I/O | https://packages.debian.org/source/buster/pyyaml | python lib, for HypersPy. |
| [sardana](https://github.com/sardana-org/sardana) | Control | https://salsa.debian.org/science-team/sardana in [SID](https://packages.debian.org/unstable/python3-sardana)    |  [F. Picca](https://salsa.debian.org/users/picca), but we are not using it at Soleil |
| sasview       | SAXS/SANS |https://salsa.debian.org/science-team/sasview  https://packages.debian.org/buster/sasview  | DebianPAN: Powder Diffraction (SAS) |
| silx          | Imaging, GUI lib|https://salsa.debian.org/science-team/silx   https://packages.debian.org/buster/silx    | DebianPAN: Data-reduction |
| spd           | Diffraction (powder) | https://packages.debian.org/source/buster/spd   | DebianPAN: Powder Diffraction, superseeded by pyfai |
| skimage       | Imaging | https://salsa.debian.org/science-team/skimage  https://packages.debian.org/buster/python3-skimage  | DebianPAN: Microscopy |
| tango         | Control |https://salsa.debian.org/science-team/tango  https://packages.debian.org/buster/tango-test   | DebianPAN: control-systems |
| [taurus](https://github.com/taurus-org/taurus) | Control GUI | https://salsa.debian.org/science-team/taurus in [SID](https://packages.debian.org/unstable/python3-taurus) |  [F. Picca](https://salsa.debian.org/users/picca), but we are not using it at Soleil |
| [taurus-pyqtgraph](https://github.com/taurus-org/taurus_pyqtgraph) | Control GUI| https://salsa.debian.org/science-team/taurus in [SID](https://packages.debian.org/sid/python3-taurus-pyqtgraph) |  [F. Picca](https://salsa.debian.org/users/picca), but we are not using it at Soleil |
| ufo-core      | Tomo | https://salsa.debian.org/science-team/ufo-core https://packages.debian.org/buster/libufo-data  | DebianPAN: Tomography |
| ufo-filters   | Tomo | https://salsa.debian.org/science-team/ufo-filters https://packages.debian.org/buster/ufo-filters | DebianPAN: Tomography |
| veusz         | 3D Viz | https://packages.debian.org/sid/veusz | 3D |
| vistrails     | GUI | https://salsa.debian.org/science-team/vistrails https://packages.debian.org/buster/vistrails | DebianPAN: Data-reduction :warning: STOPPED project |
| python3-fisx  | XRF | https://packages.debian.org/source/sid/python-fisx | X-ray fluorescence (PyMCA) |
